package TestNG_Projects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class Project1 {
	WebDriver driver;

	@BeforeTest
	public void url() {
		driver = new FirefoxDriver();
		driver.get("https://alchemy.hguy.co/jobs");
	}

	@Test
	public void title() {
		/*
		 * driver = new FirefoxDriver(); driver.get("https://alchemy.hguy.co/jobs");
		 */
		String titleName = driver.getTitle();
		Assert.assertEquals(titleName, "Alchemy Jobs – Job Board Application");
		System.out.println("title of the page is :" + titleName);

	}

	@Test
	public void heading() {
		WebElement headingName = driver.findElement(By.xpath("//h1[text()='Welcome to Alchemy Jobs']"));
		Assert.assertEquals(headingName.getText(), "Welcome to Alchemy Jobs");
		System.out.println("heading of the page is :" + headingName.getText());
	}

	@Test
	public void urlHeader() {
		WebElement headingUrl = driver.findElement(By.xpath("//img[contains(@src,'https')]"));
		System.out.println("heading of the URL :" + headingUrl.getAttribute("src"));
		System.out.println("heading of the URL :" + driver.getCurrentUrl());
	}

	@Test
	public void Header2() {
		WebElement headingName2 = driver.findElement(By.tagName("h2"));
		Assert.assertEquals(headingName2.getText(), "Quia quis non");
		System.out.println("heading of the page is :" + headingName2.getText());
	}

	@Test
	public void navigate() {
		WebElement jobs = driver.findElement(By.xpath("(//a[contains(@href,'https')])[2]"));
		jobs.click();
		// Assert.assertEquals(jobs.getText(), "Jobs");
		System.out.println(" page title of Jobs :" + driver.getTitle());
		Assert.assertEquals(driver.getTitle(), "Jobs – Alchemy Jobs");
	}



	@AfterTest
	public void close() {
		//driver.close();
	}

}
